import 'package:flutter/material.dart';
import 'package:smart_home_app/components/Theme.dart';
import 'drawer.dart';


class CustomAppBar extends StatelessWidget with PreferredSizeWidget {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Color(0xFF03a54f),
      elevation: 0.0,
      leading: IconButton(
        icon: const Icon(Icons.menu, color: Colors.white,),
        iconSize: 28.0,
        onPressed: () {   
                  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => DrawerScreen()),
  );
        },
      ),
      actions: <Widget>[
        IconButton(
          icon: const Icon(Icons.notifications_none, color: Colors.white),
          iconSize: 28.0,
          onPressed: () {},
        ),
      ],
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}
