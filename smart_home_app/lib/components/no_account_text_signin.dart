import 'package:flutter/material.dart';
import 'package:smart_home_app/screens/client_signin/client_sign_in_screen.dart';

import '../constants.dart';
import '../size_config.dart';

class NoAccountTextSignIn extends StatelessWidget {
  const NoAccountTextSignIn({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "Already have an account? ",
          style: TextStyle(fontSize: getProportionateScreenWidth(16)),
        ),
        GestureDetector(
          onTap: () => Navigator.pushNamed(context, ClientSignInScreen.routeName),
          child: Text(
            "Sign In",
            style: TextStyle(
                fontSize: getProportionateScreenWidth(16),
                color: kPrimaryColor),
          ),
        ),
      ],
    );
  }
}
