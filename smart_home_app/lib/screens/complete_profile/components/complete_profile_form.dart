import 'package:flutter/material.dart';
import 'package:smart_home_app/components/custom_surfix_icon.dart';
import 'package:smart_home_app/components/default_button.dart';
import 'package:smart_home_app/components/form_error.dart';
import 'package:smart_home_app/screens/otp/otp_screen.dart';

import '../../../constants.dart';
import '../../../size_config.dart';

class CompleteProfileForm extends StatefulWidget {
  @override
  _CompleteProfileFormState createState() => _CompleteProfileFormState();
}

class _CompleteProfileFormState extends State<CompleteProfileForm> {
  final _formKey = GlobalKey<FormState>();
  final List<String> errors = [];
  String firstName;
  String lastName;
  String phoneNumber; 
  String location;
  String stateOfOrigin;
  String practiceYears;
  String skills;
  String designation;


  void addError({String error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          buildFirstNameFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildLastNameFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildPhoneNumberFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildLocationFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildStateOfOriginFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildYearsOfPracticeFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildSkillsFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildDesignationFormField(),
          FormError(errors: errors),
          SizedBox(height: getProportionateScreenHeight(40)),
          DefaultButton(
            text: "continue",
            press: () {
              if (_formKey.currentState.validate()) {
                Navigator.pushNamed(context, OtpScreen.routeName);
              }
            },
          ),
        ],
      ),
    );
  }
  TextFormField buildDesignationFormField() {
    return TextFormField(
      onSaved: (newValue) => stateOfOrigin = newValue,
      decoration: InputDecoration(
        labelText: "Designation",
        hintText: "Enter your Designation",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomPrefixIcon(svgIcon: "assets/icons/Parcel.svg"),
      ),
    );
  }
     TextFormField buildSkillsFormField() {
    return TextFormField(
      onSaved: (newValue) => stateOfOrigin = newValue,
      decoration: InputDecoration(
        labelText: "Skills",
        hintText: "Select Skills",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomPrefixIcon(svgIcon: "assets/icons/Gift Icon.svg"),
      ),
    );
  }
    TextFormField buildYearsOfPracticeFormField() {
    return TextFormField(
      onSaved: (newValue) => stateOfOrigin = newValue,
      decoration: InputDecoration(
        labelText: "Years of Practice",
        hintText: "Enter your years of practice",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomPrefixIcon(svgIcon: "assets/icons/Star Icon.svg"),
      ),
    );
  }
  TextFormField buildStateOfOriginFormField() {
    return TextFormField(
      onSaved: (newValue) => stateOfOrigin = newValue,
      decoration: InputDecoration(
        labelText: "State of Origin",
        hintText: "Enter your State",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomPrefixIcon(svgIcon: "assets/icons/User.svg"),
      ),
    );
  }
  TextFormField buildLocationFormField() {
    return TextFormField(
      onSaved: (newValue) => location = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kAddressNullError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kAddressNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "Location",
        hintText: "Enter your Location",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon:
            CustomPrefixIcon(svgIcon: "assets/icons/Location point.svg"),
      ),
    );
  }

  TextFormField buildPhoneNumberFormField() {
    return TextFormField(
      keyboardType: TextInputType.phone,
      onSaved: (newValue) => phoneNumber = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPhoneNumberNullError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kPhoneNumberNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "Phone Number",
        hintText: "Enter your phone number",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomPrefixIcon(svgIcon: "assets/icons/Phone.svg"),
      ),
    );
  }

  TextFormField buildLastNameFormField() {
    return TextFormField(
      onSaved: (newValue) => lastName = newValue,
      decoration: InputDecoration(
        labelText: "Last Name",
        hintText: "Enter your last name",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomPrefixIcon(svgIcon: "assets/icons/User.svg"),
      ),
    );
  }

  TextFormField buildFirstNameFormField() {
    return TextFormField(
      onSaved: (newValue) => firstName = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kNamelNullError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kNamelNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "First Name",
        hintText: "Enter your first name",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomPrefixIcon(svgIcon: "assets/icons/User.svg"),
      ),
    );
  }
}
