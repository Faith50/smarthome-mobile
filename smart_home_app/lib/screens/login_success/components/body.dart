import 'package:flutter/material.dart';
import 'package:smart_home_app/components/default_button.dart';
import 'package:smart_home_app/screens/home/home_screen_page.dart';
import 'package:smart_home_app/size_config.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: 
        Center(child: Column(
          children:<Widget>[

        SizedBox(height: SizeConfig.screenHeight * 0.1),
        Image.asset(
          "assets/images/success.png",
          height: SizeConfig.screenHeight * 0.2, //40%
        ),
        SizedBox(height: SizeConfig.screenHeight * 0.08),
        Text(
          "Success!",
          style: TextStyle(
            fontSize: getProportionateScreenWidth(30),
            fontWeight: FontWeight.bold,
            color: Colors.green,
          ),
        ),
         
        Spacer(),
        SizedBox(
          width: SizeConfig.screenWidth * 0.6,
          child: DefaultButton(
            text: "Back to home",
            press: () {
              Navigator.push( context, MaterialPageRoute(builder: (context) => HomeScreen()));
            },
          ),
        ),
        Spacer(),
      ])),
    );
  }
}
