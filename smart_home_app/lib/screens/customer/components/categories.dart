import 'dart:math';


import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:smart_home_app/screens/home/components/icon_btn_with_counter.dart';
import 'icon_btn_with_counter.dart';
import 'package:smart_home_app/models/Gridmodel.dart';
import 'package:smart_home_app/models/ImageSliderModel.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:carousel_slider/carousel_slider.dart';
import '../../../size_config.dart';
import 'package:smart_home_app/screens/sub_category/subcategory_screen.dart';

class Categories extends StatelessWidget {
 
  @override
  Widget build(BuildContext context) {
    
   return SingleChildScrollView(
     child: Column(
       children: [
     Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        
        OptionCard("assets/plumbing.png", 'Plumbing Services',(){
          
          Navigator.pushNamed(context, SubCategoryScreen.routeName);
        }),
        OptionCard("assets/carpenter.png", "Funiture & Wood-work",(){
          Navigator.of(context).pushNamed('/members');
        }),
        OptionCard("assets/mechanic.png", "Automobile & Generators",(){
          Navigator.of(context).pushNamed('/members');
         }),
      ],
     ),
     
 Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        
        OptionCard("assets/conditioner.png", "A/C Repairs",(){
          Navigator.of(context).pushNamed('/tracks');
        }),
        OptionCard("assets/sweeping.png", "Domestics Services",(){
          Navigator.of(context).pushNamed('/members');
        }),
        OptionCard("assets/painters.jpg", "Painters",(){
          Navigator.of(context).pushNamed('/members');
         }),
      ],
     ),

     Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        
        OptionCard("assets/Tiler.jpg", "Tiling Services",(){
          Navigator.of(context).pushNamed('/tracks');
        }),
        OptionCard("assets/welder.png", "Welding Services",(){
          Navigator.of(context).pushNamed('/members');
        }),
        OptionCard("assets/exterminator.png", "Fumigation Services",(){
          Navigator.of(context).pushNamed('/members');
         }),
      ],
     ),

     Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        
        OptionCard("assets/installation.png", "Installation Services",(){
          Navigator.of(context).pushNamed('/tracks');
        }),
        OptionCard("assets/chef.png", "Cooking Services",(){
          Navigator.of(context).pushNamed('/members');
        }),
        OptionCard("assets/repairing.png", "Electronic Repairs",(){
          Navigator.of(context).pushNamed('/members');
         }),
      ],
     ),

     Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        
        OptionCard("assets/agriculture.png", "Gardeners",(){
          Navigator.of(context).pushNamed('/tracks');
        }),
        OptionCard("assets/advertisement.png", "Cable/Media Services",(){
          Navigator.of(context).pushNamed('/members');
        }),
        OptionCard("assets/home_appliances.png", "Electronic Appliances",(){
          Navigator.of(context).pushNamed('/members');
         }),
      ],
     ),


   Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        
        OptionCard("assets/sewing.png", "Clothing & Shoes",(){
          Navigator.of(context).pushNamed('/tracks');
        }),
        OptionCard("assets/wedding-cake.png", "Food & Catering",(){
          Navigator.of(context).pushNamed('/members');
        }),
        OptionCard("assets/face-paint.png", "Beauty & Body Care",(){
          Navigator.of(context).pushNamed('/members');
         }),
      ],
     ),

        Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        
        OptionCard("assets/planner.png", "Event Planning",(){
          Navigator.of(context).pushNamed('/tracks');
        }),
        OptionCard("assets/porcelain.png", "Art & Fine Craft",(){
          Navigator.of(context).pushNamed('/members');
        }),
        OptionCard("assets/graphic-designer.png", "Computer & IT Services",(){
          Navigator.of(context).pushNamed('/members');
         }),
      ],
     ),
       ]));
   

  
  }
}
class OptionCard extends StatelessWidget{

  final String title;
  final String asset;
 final GestureTapCallback _onTap;

  OptionCard(this.asset, this.title,  this._onTap);

  @override
  Widget build(BuildContext context) {
    return Container(
      
      width: 100,
      height: 90,
      margin: EdgeInsets.all(8),
      child: Card(
        elevation: 1.0,
        
        margin: EdgeInsets.all(0),
        
        child: InkWell(
          onTap: _onTap,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                  SizedBox(
                   width: 30,
                height: 30,
                    child: Image.asset(
                      asset,
                      fit: BoxFit.fitWidth,
                    ),
                  ),
                Spacer(),
                Padding(
                padding: const EdgeInsets.only(top: 5),
                child:
                
                Text(title, textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 12),))
              ],

            ),
          ),
        ),
      ),
    );
  }
}