import 'dart:math';


import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:smart_home_app/screens/home/components/icon_btn_with_counter.dart';
import 'icon_btn_with_counter.dart';
import 'package:smart_home_app/models/Gridmodel.dart';
import 'package:smart_home_app/models/ImageSliderModel.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:carousel_slider/carousel_slider.dart';
import '../../../size_config.dart';

class Category extends StatelessWidget {
 
  @override
  Widget build(BuildContext context) {
    
   return SingleChildScrollView(
     
      child: new Column(
        children: <Widget>[
          Container(
              width: double.maxFinite,
              color: Colors.blue[800],
              child: Container(
                
              )
          ),


          Container( color: Colors.blue[800],
            
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 1),
           
          ),
          GridView.count(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            crossAxisCount: 3,
            children: List<GridItem>.generate(24, (int index) {
              return GridItem(_getGridItemList()[index]);
              
            }),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 1, bottom: 5),
            child: Container(
              color: Colors.green,
              
            ),
          ),
        ],
      ),
    );
  }

  List<GridModel> _getGridItemList() {
    List<GridModel> list = new List<GridModel>();
    list.add(new GridModel("assets/electrician.png", "Electrical Services", null)
    );
    list.add(new GridModel("assets/plumbing.png", "Plumbing Services", null));
    list.add(new GridModel("assets/carpenter.png", "Funiture & Wood-work", null));
    list.add(new GridModel("assets/mechanic.png", "Automobile & Generators", null));
    list.add(new GridModel("assets/conditioner.png", "A/C Repairs", null));
    list.add(new GridModel("assets/sweeping.png", "Domestics Services", null));
    list.add(new GridModel("assets/painters.jpg", "Painters", null));
    list.add(new GridModel("assets/painter.png", "Painting Services", null));
    list.add(new GridModel("assets/Tiler.jpg", "Tiling Services", null));
    list.add(new GridModel("assets/welder.png", "Welding Services", null));
    list.add(new GridModel("assets/exterminator.png", "Fumigation Services", null));
    list.add(new GridModel("assets/installation.png", "Installation Services", null));
    list.add(new GridModel("assets/chef.png", "Cooking Services", null));
    list.add(new GridModel("assets/repairing.png", "Electronic Repairs", null));
    list.add(new GridModel("assets/agriculture.png", "Gardeners", null));
    list.add(new GridModel("assets/advertisement.png", "Cable/Media Services", null),);
    list.add(new GridModel("assets/home_appliances.png", "Electronic Appliances", null));
    list.add(new GridModel("assets/sewing.png", "Clothing & Shoes", null),);
    list.add(new GridModel("assets/wedding-cake.png", "Food & Catering", null));
    list.add(
      new GridModel("assets/face-paint.png", "Beauty & Body Care", null),
    );
       list.add(new GridModel("assets/audio-engineer.png", "Sound Engineers", null));
    list.add(
      new GridModel("assets/porcelain.png", "Art & Fine Craft", null),
    );
       list.add(new GridModel("assets/graphic-designer.png", "Computer & IT Services", null));
    list.add(
      new GridModel("assets/planner.png", "Event Planning", null),
    );
        
    return list;
  }

  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }
    return result;
  }

 
}
class GridLayout {
  final String title;
  final IconData icon;

  GridLayout({this.title, this.icon});
}

class GridItem extends StatelessWidget {
  GridModel gridModel;

  GridItem(this.gridModel);

  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: 1.0,
        child: Container(
        color: Colors.white,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              
              Image.asset(
                gridModel.imagePath,
                width: 30,
                height: 30,
                color: gridModel.color,
                
              ),
              
             
              Padding(
                padding: const EdgeInsets.only(top: 5),
                child: Text(
                  gridModel.title,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 12),
                ),
                
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class GridItemTop extends StatelessWidget {
  GridModel gridModel;

  GridItemTop(this.gridModel);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(1 / 2),
      child: Container(

        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Image.asset(
                gridModel.imagePath,
                width: 30,
                height: 30,
                color: gridModel.color,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 5),
                child: Text(
                  gridModel.title,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 12, color: Colors.white,),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
