import 'package:flutter/material.dart';
import 'package:smart_home_app/components/coustom_bottom_nav_bar.dart';
import 'package:smart_home_app/enums.dart';

import 'components/body.dart';

class CustomerHomeScreen extends StatelessWidget {
  static String routeName = "/customer";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        //leading: Icon(Icons.menu, color: Color(0xFFffffff)),
        title: Text("Smart Homes", style: TextStyle(color: Colors.white),), 
        backgroundColor: Color(0xFF02a651),
      
      ),
       drawer: Drawer(
           
        child: ListView(
          
          padding: EdgeInsets.all(0),
          children: <Widget>[
            UserAccountsDrawerHeader(
               decoration: BoxDecoration(
          color: Color(0xFF03a54f),
        ),
              accountEmail: Text("faith@gmail.com"),
              accountName: Text("Faith Godwin"),
              currentAccountPicture: CircleAvatar(
                  radius: 80.0,
                      backgroundColor: const Color(0xFF03a54f),
                      child: Image.asset(
                       "assets/images/smart-homes-logo-white.png",
                      ),
              ),
              
            ),
            ListTile(
              leading: Icon(Icons.home, color: Color(0xFF03a54f)),
              title: Text("Home", style: TextStyle(color: Color(0xFF03a54f)),),
              onTap: (){},
            ),
            ListTile(
              leading: Icon(Icons.people, color: Color(0xFF03a54f)),
              title: Text("My Profile", style: TextStyle(color: Color(0xFF03a54f)),),
              onTap: (){},
            ),
            ListTile(
              leading: Icon(Icons.folder, color: Color(0xFF03a54f)),
              title: Text("Wallet", style: TextStyle(color: Color(0xFF03a54f)),),
              onTap: (){},
            ),
            ListTile(
              leading: Icon(Icons.room_service, color: Color(0xFF03a54f)),
              title: Text("Pending Service", style: TextStyle(color: Color(0xFF03a54f)),),
              onTap: (){},
            ),
            ListTile(
              leading: Icon(Icons.done_all, color: Color(0xFF03a54f)),
              title: Text("Complete Services", style: TextStyle(color: Color(0xFF03a54f)),),
              onTap: (){},
            ),
            ListTile(
              leading: Icon(Icons.rate_review, color: Color(0xFF03a54f)),
              title: Text("Ratings", style: TextStyle(color: Color(0xFF03a54f)),),
              onTap: (){},
            ),
            ListTile(
              leading: Icon(Icons.settings, color: Color(0xFF03a54f)),
              title: Text("Settings", style: TextStyle(color: Color(0xFF03a54f)),),
              onTap: (){},
            ),
            ListTile(
              leading: Icon(Icons.logout, color: Color(0xFF03a54f)),
              title: Text("Logout", style: TextStyle(color: Color(0xFF03a54f)),),
              onTap: (){},
            ),
          ],
        ),
      ),
      body: Body(),
      bottomNavigationBar: CustomBottomNavBar(selectedMenu: MenuState.customer),
    );
  }
}
