import 'package:flutter/material.dart';
import 'package:smart_home_app/constants.dart';
import 'package:smart_home_app/screens/screen1/screen.dart';
import 'package:smart_home_app/size_config.dart';
import 'dart:async';

// This is the best practice
import 'package:smart_home_app/screens/secondsplash/second_splash_screen.dart';
import '../../../components/default_button.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
Widget build(BuildContext context) {
    return initScreen(context);
  }


  @override
  void initState() {
    super.initState();
    startTimer();
  }

  startTimer() async {
    var duration = Duration(seconds: 7);
    return new Timer(duration, route);
  }

  route() {
    Navigator.pushReplacement(context, MaterialPageRoute(
      builder: (context) => 
        SecondSplashScreen()
      )
    );
  }

  initScreen(BuildContext context) {

    return Scaffold(
      backgroundColor: Color(0xFF02a651),
      
      body: Center(
        
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
             Padding(padding: EdgeInsets.only(top: 60.0)),
             CircleAvatar(
                        backgroundColor: Color(0xFF02a651),
                        radius: 80.0,
            
              child: Image.asset("assets/images/smart-homes-logo-white.png"),
            ),
           
            Padding(padding: EdgeInsets.only(top: 160.0)),
            CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(Color(0xFFffffff)),),
         //   CircularProgressIndicator(
            //  backgroundColor: Color(0xFFffffff),
            //  strokeWidth: 5,
           // )
          ],
        ),
      ),
    );
  }
}
