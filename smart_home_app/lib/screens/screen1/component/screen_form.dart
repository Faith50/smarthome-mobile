import 'package:flutter/material.dart';
import 'package:smart_home_app/screens/screen1/screen.dart';
import 'package:smart_home_app/size_config.dart';

// This is the best practice
import '../../../components/default_button.dart';
import 'package:smart_home_app/screens/client_signup/client_sign_up_screen.dart';
import 'package:smart_home_app/screens/sign_in/sign_in_screen.dart';
import '../../../size_config.dart';

 class ScreenForm extends StatefulWidget {
  @override
  _ScreenFormState createState() => new _ScreenFormState();
}

class _ScreenFormState extends State<ScreenForm>with TickerProviderStateMixin {
      @override
  void initState() {
    super.initState();
  }

Widget build(BuildContext context){
    return new Container(
      height: MediaQuery.of(context).size.height,
    
      child: new Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 50.0),
            child: Center(
              child: Image.asset(
          "assets/images/smart-homes-logo.png",
          height: SizeConfig.screenHeight * 0.2, //40%
        ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 20.0),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                 SizedBox(height: SizeConfig.screenHeight * 0.1),
                Text(
                  "Continue as",
                  style: TextStyle(
                    color: Color(0xFF03a54f),
                    fontSize: 20.0,
                  ),
                ),
                
              ],
            ),
          ),
          new Container(
            width: MediaQuery.of(context).size.width,
            margin: const EdgeInsets.only(left: 30.0, right: 30.0, top: 150.0),
            alignment: Alignment.center,
            child: new Row(
              children: <Widget>[
                new Expanded(
                  child: FlatButton(
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(30.0)),
                    color: Color(0xFF03a54f),
                   onPressed: () => {
                     Navigator.pushNamed(context, ClientSignUpScreen.routeName),
                   },
                    child: new Container(
                      padding: const EdgeInsets.symmetric(
                        vertical: 20.0,
                        horizontal: 20.0,
                      ),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Expanded(
                            child: Text(
                              "CUSTOMER",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          new Container(
            width: MediaQuery.of(context).size.width,
            margin: const EdgeInsets.only(left: 30.0, right: 30.0, top: 30.0),
            alignment: Alignment.center,
            child: new Row(
              children: <Widget>[
                new Expanded(
                  child: FlatButton(
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(30.0)),
                    color: Color(0xFF03a54f),
                    onPressed: () {
                      Navigator.pushNamed(context, SignInScreen.routeName);
                    },
                    child: new Container(
                      padding: const EdgeInsets.symmetric(
                        vertical: 20.0,
                        horizontal: 20.0,
                      ),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Expanded(
                            child: Text(
                              "ARTISAN",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),

        ],
      ),
    );
  }}
