import 'package:flutter/material.dart';
import 'package:smart_home_app/components/no_account_text.dart';
import 'package:smart_home_app/components/socal_card.dart';
import '../../../size_config.dart';
import 'screen_form.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          constraints: BoxConstraints.expand(),
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/smarthome-bg.png"),
              fit: BoxFit.cover,
            ),
          ),
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: SizeConfig.screenHeight * 0.04),
             
                ScreenForm(),
                   
              ],
            ),
          ),
        ),
      ),
    ));
  }
}
