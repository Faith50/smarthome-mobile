import 'package:flutter/material.dart';
import 'package:smart_home_app/components/coustom_bottom_nav_bar.dart';
import 'package:smart_home_app/enums.dart';

import 'components/body.dart';

class ProfileScreen extends StatelessWidget {
  static String routeName = "/profile";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Icon(Icons.arrow_back, color: Colors.white),
      centerTitle: false,       
        backgroundColor: Color(0xFF03a54f),
        title: Text("Profile", style: TextStyle(color: Colors.white, fontSize: 20)),
      ),
      body: Body(),
      //bottomNavigationBar: CustomBottomNavBar(selectedMenu: MenuState.profile),
    );
  }
}
