import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
//import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';


class ProfilePic extends StatefulWidget {
  ProfilePic({Key key}) : super(key: key);

  @override
  _ProfilePicState createState() => _ProfilePicState();
}

class _ProfilePicState extends State<ProfilePic> {
 File imageFile;

_openGallary(BuildContext context) async{
  var picture = await ImagePicker.pickImage (source: ImageSource.gallery);
  this.setState((){
imageFile = picture;
  });
Navigator.of(context).pop();
}
_openCamera(BuildContext context) async{
   var picture = await ImagePicker.pickImage(source: ImageSource.camera);
   this.setState((){
     imageFile = picture;
   }); 
Navigator.of(context).pop();
}
  Future<void> _showChoiceDialog(BuildContext context){
    return showDialog(context: context, builder: (BuildContext context){
return AlertDialog(
      title: Text("Choose profile pix"),
  content: SingleChildScrollView(
child: ListBody(
children: <Widget>[
  GestureDetector(
    child: Text("Gallary"),
    onTap:(){
      _openGallary(context);
    }
  ),
  Padding(
    padding: EdgeInsets.all(8.0)
  ),
  GestureDetector(
    child: Text("Camera"),
    onTap:(){
      _openCamera(context);
    }
  ),
]
),
  ),
);
    });
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 115,
      width: 115,
      child: Stack(
        fit: StackFit.expand,
        overflow: Overflow.visible,
        children: [
          CircleAvatar(
             backgroundColor: Color(0xFF03a54f),
            radius: 80,
              child: imageFile == null ? Text("Picture", style: TextStyle(color: Colors.white)) : null,
              backgroundImage:
                  imageFile != null ? FileImage(imageFile) : null,

          ),
          Positioned(
            right: -16,
            bottom: 0,
            child: SizedBox(
              height: 46,
              width: 46,
              child: FlatButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50),
                  side: BorderSide(color: Colors.white),
                ),
                color: Color(0xFFF5F6F9),
                onPressed: () {
                  _showChoiceDialog(context);

                },
                child: SvgPicture.asset("assets/icons/Camera Icon.svg"),
              ),
            ),
          )
        ],
      ),
    );
  }
}
