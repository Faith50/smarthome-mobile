import 'package:flutter/material.dart';

import 'components/body.dart';

class ClientSignInScreen extends StatelessWidget {
  static String routeName = "/client_signin";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        
        title: Text("Sign In"),flexibleSpace: Image(
          image: AssetImage("assets/images/smarthome-bg.png"),
          fit: BoxFit.cover,
        ),
        backgroundColor: Colors.transparent,
      
      ),
      body: Body(),
    );
  }
}
