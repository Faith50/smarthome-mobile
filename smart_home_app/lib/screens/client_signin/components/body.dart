import 'package:flutter/material.dart';
import 'package:smart_home_app/components/no_account_text.dart';
import 'package:smart_home_app/components/socal_card.dart';
import '../../../size_config.dart';
import 'client_sign_form.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
          constraints: BoxConstraints.expand(),
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/smarthome-bg.png"),
              fit: BoxFit.cover,
            ),
          ),
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: SizeConfig.screenHeight * 0.04),
                Image.asset(
          "assets/images/smart-homes-logo.png",
          height: SizeConfig.screenHeight * 0.2, //40%
        ),
                Text(
                  "Sign in with your email and password ",
                  textAlign: TextAlign.center, style: TextStyle(color: Color(0xFF03a54f))
                ),
                SizedBox(height: SizeConfig.screenHeight * 0.08),
                ClientSignForm(),
                SizedBox(height: SizeConfig.screenHeight * 0.04),
                Text(
                  " OR ",
                  textAlign: TextAlign.center, style: TextStyle(color: Color(0xFF03a54f))
                ),
                Text(
                  " Continue with social media",
                  textAlign: TextAlign.center, style: TextStyle(color: Color(0xFF03a54f))
                ),
                SizedBox(height: SizeConfig.screenHeight * 0.04),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SocalCard(
                      icon: "assets/icons/google-icon.svg", 
                      press: () {},
                    ),
                    SocalCard(
                      icon: "assets/icons/facebook-2.svg",
                      press: () {},
                    ),
                    SocalCard(
                      icon: "assets/icons/twitter.svg",
                      press: () {},
                    ),
                  ],
                ),
                SizedBox(height: getProportionateScreenHeight(20)),
                NoAccountText(),
              ],
            ),
          ),
        ),
      ),
    ));
  }
}
