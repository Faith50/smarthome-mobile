import 'package:flutter/material.dart';
import 'package:smart_home_app/screens/cart/cart_screen.dart';

import '../../../size_config.dart';
import 'category_search_field.dart';

class SubCategoryList extends StatelessWidget {
  const SubCategoryList({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          CategorySearchField(),
          ],
      ),
    );
  }
}
