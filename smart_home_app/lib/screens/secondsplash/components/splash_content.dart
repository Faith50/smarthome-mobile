import 'package:flutter/material.dart';

import '../../../constants.dart';
import '../../../size_config.dart';

class SplashContent extends StatelessWidget {
  const SplashContent({
    Key key,
    this.text,
    this.image,
  }) : super(key: key);
  final String text, image;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Spacer(),
        SizedBox(height: SizeConfig.screenHeight * 0.02),
                Image.asset(
          "assets/images/smart-homes-logo.png",
          height: SizeConfig.screenHeight * 0.18,),
        Text(
          text,
          textAlign: TextAlign.center,
        ),
        Spacer(flex: 2),
        Image.asset(
          image,
          height: getProportionateScreenHeight(255),
          width: getProportionateScreenWidth(225),
        ),
      ],
    );
  }
}
