import 'package:flutter/material.dart';
import 'package:smart_home_app/screens/secondsplash/components/body.dart';
import 'package:smart_home_app/size_config.dart';

class SecondSplashScreen extends StatelessWidget {
  static String routeName = "/secondsplash";
  @override
  Widget build(BuildContext context) {
    // You have to call it on your starting screen
    SizeConfig().init(context);
    return Scaffold(
      body: Body(),
    );
  }
}
