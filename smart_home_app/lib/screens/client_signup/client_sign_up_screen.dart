import 'package:flutter/material.dart';

import 'components/body.dart';

class ClientSignUpScreen extends StatelessWidget {
  static String routeName = "/client_signup";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
         iconTheme: IconThemeData(color: Colors.green),
        title: Text("Sign Up"), flexibleSpace: Image(
          image: AssetImage("assets/images/smarthome-bg.png"),
          fit: BoxFit.cover,
        ),
        backgroundColor: Colors.transparent,
      
      ),
      body: Body(),
      
    );
  }
}
