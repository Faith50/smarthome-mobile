import 'package:flutter/material.dart';
import 'package:smart_home_app/components/socal_card.dart';
import 'package:smart_home_app/constants.dart';
import 'package:smart_home_app/size_config.dart';
import 'package:google_fonts/google_fonts.dart';
import 'client_sign_up_form.dart';
import 'package:smart_home_app/components/no_account_text_signin.dart';


class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
          constraints: BoxConstraints.expand(),
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/smarthome-bg.png"),
              fit: BoxFit.cover,
            ),
          ),
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: SingleChildScrollView(
            child: Column(
              children: [
                  
                Image.asset(
          "assets/images/smart-homes-logo.png",
          height: SizeConfig.screenHeight * 0.13, //40%
        ),
                //SizedBox(height: SizeConfig.screenHeight * 0.04), // 4%
                Text("Register as Customer",  style: GoogleFonts.lato(fontStyle: FontStyle.normal, color: Colors.black45, textStyle: headingStyle),),
              Text(
                  "Enter your details below",
                  textAlign: TextAlign.center, style: GoogleFonts.lato(fontStyle: FontStyle.normal, color: Color(0xFF03a54f))
                ),
                SizedBox(height: SizeConfig.screenHeight * 0.08),
                ClientSignUpForm(),
                SizedBox(height: SizeConfig.screenHeight * 0.04),
                   Text(
                  "OR", style: GoogleFonts.lato(fontStyle: FontStyle.normal, ),
                  textAlign: TextAlign.center,
                ),
                
                   Text(
                  "Continue with social media", style: GoogleFonts.lato(fontStyle: FontStyle.normal),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: SizeConfig.screenHeight * 0.04),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  
                  children: [
                    
                    SocalCard(
                      icon: "assets/icons/google-icon.svg",
                      press: () {},
                    ),
                    SocalCard(
                      icon: "assets/icons/facebook-2.svg",
                      press: () {},
                    ),
                    SocalCard(
                      icon: "assets/icons/twitter.svg",
                      press: () {},
                    ),
                  ],
                ),
                SizedBox(height: getProportionateScreenHeight(20)),
                NoAccountTextSignIn(),
              ],
            ),
          ),
        ),
      ),
    ));
  }
}
