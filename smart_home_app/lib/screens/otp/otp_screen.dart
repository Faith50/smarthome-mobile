import 'package:flutter/material.dart';
import 'package:smart_home_app/size_config.dart';

import 'components/body.dart';

class OtpScreen extends StatelessWidget {
  static String routeName = "/otp";
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.green),
        title: Text(""), flexibleSpace: Image(
          image: AssetImage("assets/images/smarthome-bg.png"),
          fit: BoxFit.cover,
        ),
      ),
      body: Body(),
    );
  }
}
