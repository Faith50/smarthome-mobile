import 'package:flutter/material.dart';
import 'package:smart_home_app/constants.dart';
import 'package:smart_home_app/size_config.dart';
import 'package:google_fonts/google_fonts.dart';

import 'otp_form.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
          constraints: BoxConstraints.expand(),
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/smarthome-bg.png"),
              fit: BoxFit.cover,
            ),
          ),
    
    child: SizedBox(
      width: double.infinity,
      child: Padding(
        padding:
        EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
        child: SingleChildScrollView(
          child: Column(
            children: [
                  
              SizedBox(height: SizeConfig.screenHeight * 0.05),
              Text(
                "OTP Verification", style: GoogleFonts.lato(fontStyle: FontStyle.normal, color: Colors.black45, textStyle: headingStyle),),
               
              
              Text("We sent your code to +234 ***"),
              buildTimer(),
              OtpForm(),
              SizedBox(height: SizeConfig.screenHeight * 0.1),
              GestureDetector(
                onTap: () {
                  // OTP code resend
                },
                child: Text(
                  "Resend OTP Code",
                  style: TextStyle(decoration: TextDecoration.underline),
                ),
              )
            ],
          ),
        ),
      ),
    ));
  }

  Row buildTimer() {                                                                                  
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text("This code will expired in "),
        TweenAnimationBuilder(
          tween: Tween(begin: 30.0, end: 0.0),
          duration: Duration(seconds: 30),
          builder: (_, value, child) => Text(
            "00:${value.toInt()}",
            style: TextStyle(color: kPrimaryColor),
          ),
        ),
      ],
    );
  }
}
