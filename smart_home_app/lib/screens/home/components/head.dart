import 'package:flutter/material.dart';
import 'package:smart_home_app/config/palette.dart';
import 'package:smart_home_app/config/styles.dart';
import 'package:smart_home_app/data/data.dart';
import 'package:smart_home_app/widgets/widgets.dart';
import 'package:smart_home_app/screens/jobs/components/upload.dart';

import 'package:google_fonts/google_fonts.dart';
import 'package:get/get.dart';
import '../../../size_config.dart';


class HeadScreen extends StatefulWidget {
  @override
  _HeadScreenState createState() => _HeadScreenState();
}

class _HeadScreenState extends State<HeadScreen> {
  //Indicator Handler
  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }
    return result;
  }

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: CustomAppBar(),
      body: CustomScrollView(
        physics: ClampingScrollPhysics(),
        slivers: <Widget>[
          _buildHeadScreen(screenHeight),
          //(screenHeight),
          //_buildBottom(screenHeight),
          //_buildYourOwnTest(screenHeight),
        ],
      ),
    );
  }

  SliverToBoxAdapter _buildHeadScreen(double screenHeight) {
    return SliverToBoxAdapter(
      child: Container(
        padding: const EdgeInsets.all(20.0),
        decoration: BoxDecoration(
          color: Color(0xFF03a54f),
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(40.0),
            bottomRight: Radius.circular(40.0),
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
              Image.asset(
                "assets/images/smart-homes-logo-white.png",
                width: Get.width * 0.3,
                height: Get.height * 0.2,
              ),
            ]),
          ],
        ),
      ),
    );
  }

}

