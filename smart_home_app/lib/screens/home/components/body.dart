import 'package:flutter/material.dart';
import 'package:smart_home_app/widgets/drawer.dart';
import 'package:smart_home_app/widgets/navbar.dart';
import 'package:smart_home_app/components/Theme.dart';
import '../../../size_config.dart';
import 'categories.dart';
import 'completed_jobs.dart';
import 'discount_banner.dart';
import 'head.dart';
import 'popular_product.dart';
import 'special_offers.dart';
import 'toggle_button.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
       
        body: SafeArea(
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: getProportionateScreenHeight(20)),
            HeadScreen(),
            SizedBox(height: getProportionateScreenWidth(10)),
            ToggleButton(),
            SizedBox(height: getProportionateScreenHeight(20)),
            ToggleButton(),
            //CompletedJobs(),
            
            SizedBox(height: getProportionateScreenWidth(30)),
            SpecialOffers(),
            SizedBox(height: getProportionateScreenWidth(30)),
            SpecialOffers(),
            SizedBox(height: getProportionateScreenWidth(30)),
          ],
        ),
      ),
    ));
  }
}
