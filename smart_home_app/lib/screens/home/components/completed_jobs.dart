import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../size_config.dart';
import 'section_title.dart';

class CompletedJobs extends StatelessWidget {
  const CompletedJobs({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(children: [
    Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(15)),
          child: SingleChildScrollView(
            child: Column(
              children: [
            
                  Container(
                  height: 400.0,
                
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(15),
                          topRight: Radius.circular(15), )),
                  child: Padding(
                    padding: const EdgeInsets.all(5.0),
                    child:  Container(
          padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 2.0),
          child: GridView.count(
            crossAxisCount: 2,
            padding: EdgeInsets.all(3.0),
            children: <Widget>[
              DashboardItem("New Jobs", Icons.handyman),
              DashboardItem("Pending Jobs", Icons.pending,),
              DashboardItem("Accepted Jobs", Icons.approval),
              DashboardItem("Completed Jobs", Icons.done),
              DashboardItem("Cancel Request", Icons.cancel),
              DashboardItem("New Messages", Icons.message),
            ],
          ),
        ),
                
                  ))],
            ),
          ),
        ),
    ]);
 
  }

  Card DashboardItem(String title, IconData icon) {
    return Card(
        elevation: 1.0,
        margin: new EdgeInsets.all(3.0),
        child: Container(
          decoration: BoxDecoration(
            boxShadow: [
          BoxShadow(
             offset: Offset(5, 5),
            color: Colors.black38,
            blurRadius: 1.0,          
          ),
          
        ],
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          stops: [0.3, 1],
          colors: [Color(0xffffffff), Color(0xffffffff)]
          ),
           borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(10.0),
            bottomRight: Radius.circular(10.0),
          ),
          ),
          child: new InkWell(
            onTap: () {},
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.min,
              verticalDirection: VerticalDirection.down,
              children: <Widget>[
                SizedBox(height: 50.0),
                Center(
                    child: Icon(
                  icon,
                  size: 40.0,
                  color: Color(0xFF03a54f),
                )),
                SizedBox(height: 20.0),
                 Center(
                  child:  Text(title,
                      style:
                           TextStyle(fontSize: 12.0, color: Color(0xFF03a54f), fontWeight: FontWeight.w700)),
                )
              ],
            ),
          ),
        ));
  }
  
}
