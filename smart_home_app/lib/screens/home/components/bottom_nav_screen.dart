import 'package:flutter/material.dart';
//import 'package:smart_home_app/screens/screens.dart';

class BottomNavigationWidget extends StatefulWidget{
  @override
  State createState() {
    // TODO: implement createState
    return BottomNavigationWidgetState();
  }

}
class BottomNavigationWidgetState extends State
{
  final _bottomNavigationColor = Colors.white;
  int _currentIndex = 0;
  List list = List();

  @override
  void initState() {
    
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.grey,
        body:list[_currentIndex],
        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Colors.pink,
          items: [
          BottomNavigationBarItem(
              icon: Icon(
                Icons.home,
                color: _bottomNavigationColor,
              ),
              title: Text(
                'HOME',
                style: TextStyle(color: _bottomNavigationColor,fontSize: 12),
              ),

          ), BottomNavigationBarItem(
              icon: Icon(
                Icons.search,
                color: _bottomNavigationColor,
              ),
              title: Text(
                'SEARCH',
                style: TextStyle(color: _bottomNavigationColor,fontSize: 12),
              )
          ),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.library_books,
                color: _bottomNavigationColor,
              ),
              title: Text(
                'LIBRARY',
                style: TextStyle(color: _bottomNavigationColor,fontSize: 12),
              )
          ),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.account_circle,
                color: _bottomNavigationColor,
              ),
              title: Text(
                'PROFILE',
                style: TextStyle(color: _bottomNavigationColor,fontSize: 12),
              )
          ),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.book,
                color: _bottomNavigationColor,
              ),
              title: Text(
                'MY COURSES',
                style: TextStyle(color: _bottomNavigationColor,fontSize: 12),
              )
          ),

        ],currentIndex: _currentIndex,
          onTap: (int index){
          setState(() {
            _currentIndex=index;
          });

        }, type: BottomNavigationBarType.fixed,),
      ),
    );
  }

}
  

