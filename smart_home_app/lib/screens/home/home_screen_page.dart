import 'package:flutter/material.dart';
import 'package:smart_home_app/widgets/drawer.dart';
import 'package:smart_home_app/components/coustom_bottom_nav_bar.dart';
import 'components/toggle_button.dart';

import 'components/special_offers.dart';
import 'components/completed_jobs.dart';

import 'package:get/get.dart';
import '../../../size_config.dart';


class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  //Indicator Handler
  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }
    return result;
  }

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
       appBar: AppBar(
         iconTheme: IconThemeData(color: Colors.white),
        // leading: Icon(Icons.menu, color: Colors.white),
      centerTitle: false,
        backgroundColor: Color(0xFF03a54f),
        title: Text("Home", style: TextStyle(color: Colors.white, fontSize: 20)),
      ),
      
       drawer: Drawer(
           
        child: ListView(
          
          padding: EdgeInsets.all(0),
          children: <Widget>[
            UserAccountsDrawerHeader(
               decoration: BoxDecoration(
          color: Color(0xFF03a54f),
        ),
              accountEmail: Text("faith@gmail.com"),
              accountName: Text("Faith Godwin"),
              currentAccountPicture: CircleAvatar(
                  radius: 80.0,
                      backgroundColor: const Color(0xFF03a54f),
                      child: Image.asset(
                       "assets/images/smart-homes-logo-white.png",
                      ),
              ),
              
            ),
            ListTile(
              leading: Icon(Icons.home, color: Color(0xFF03a54f)),
              title: Text("Home", style: TextStyle(color: Color(0xFF03a54f)),),
              onTap: (){},
            ),
            ListTile(
              leading: Icon(Icons.people, color: Color(0xFF03a54f)),
              title: Text("My Profile", style: TextStyle(color: Color(0xFF03a54f)),),
              onTap: (){},
            ),
            ListTile(
              leading: Icon(Icons.folder, color: Color(0xFF03a54f)),
              title: Text("Wallet", style: TextStyle(color: Color(0xFF03a54f)),),
              onTap: (){},
            ),
            ListTile(
              leading: Icon(Icons.room_service, color: Color(0xFF03a54f)),
              title: Text("Pending Service", style: TextStyle(color: Color(0xFF03a54f)),),
              onTap: (){},
            ),
            ListTile(
              leading: Icon(Icons.done_all, color: Color(0xFF03a54f)),
              title: Text("Complete Services", style: TextStyle(color: Color(0xFF03a54f)),),
              onTap: (){},
            ),
            ListTile(
              leading: Icon(Icons.rate_review, color: Color(0xFF03a54f)),
              title: Text("Ratings", style: TextStyle(color: Color(0xFF03a54f)),),
              onTap: (){},
            ),
            ListTile(
              leading: Icon(Icons.settings, color: Color(0xFF03a54f)),
              title: Text("Settings", style: TextStyle(color: Color(0xFF03a54f)),),
              onTap: (){},
            ),
            ListTile(
              leading: Icon(Icons.logout, color: Color(0xFF03a54f)),
              title: Text("Logout", style: TextStyle(color: Color(0xFF03a54f)),),
              onTap: (){},
            ),
          ],
        ),
      ),
      body: CustomScrollView(
        physics: ClampingScrollPhysics(),
        slivers: <Widget>[
          _buildHeader(screenHeight),
          _buildBody(screenHeight),
          _buildBottom(screenHeight),
          //_buildYourOwnTest(screenHeight),
        ],
      ),
    );
  }

  SliverToBoxAdapter _buildHeader(double screenHeight) {
    return SliverToBoxAdapter(
      child: Container(
        padding: const EdgeInsets.all(10.0),
        decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        const Color(0xFF03a54f),
                        const Color(0xFF03a54f)
                      ],
                      begin: Alignment.centerLeft,
                      end: Alignment.centerRight,
                      // stops: [0.0, 0.1],
                    ), borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(30.0),
            bottomRight: Radius.circular(30.0),
          ),
                    
                    ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            
            Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
              SizedBox(height: 20),
              
                      Text(
                        "N43,729.00",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 35.0,
                            fontWeight: FontWeight.bold),
                      ),]),
                     Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                        //SizedBox(height: 20),
                      Text(
                        "Available Balance",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 15.0,
                            fontWeight: FontWeight.w300),
                      ),
             // Image.asset(
               // "assets/images/smart-homes-logo-white.png",
                //width: Get.width * 0.3,
                //height: Get.height * 0.2,
              //),
            ]),
          ],
        ),
      ),
    );
  }

  SliverToBoxAdapter _buildBody(double screenHeight) {
    return SliverToBoxAdapter(
      child: Container(
        padding: const EdgeInsets.all(5.0),                      
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
             // ToggleButton(),
            CompletedJobs(),
              
               ]),
      ),
    );
  }

   SliverToBoxAdapter _buildBottom(double screenHeight) {
    return SliverToBoxAdapter(
      child: Container(
        padding: const EdgeInsets.all(5.0),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: SizeConfig.screenHeight * 0.08),
              CustomBottomNavBar(),
            
            ]),
      ),
    );
  }
  
   
  
}
