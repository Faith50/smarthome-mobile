import 'package:flutter/material.dart';

import 'components/body.dart';

class ForgotPasswordScreen extends StatelessWidget {
  static String routeName = "/forgot_password";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Forgot Password"), flexibleSpace: Image(
          image: AssetImage("assets/images/smarthome-bg.png"),
          fit: BoxFit.cover,
        ),
        backgroundColor: Colors.transparent,
      
      ),
      body: Body(),
    );
  }
}
