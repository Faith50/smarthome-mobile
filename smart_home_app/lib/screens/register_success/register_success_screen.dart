import 'package:flutter/material.dart';

import 'components/body.dart';

class RegisterSuccessScreen extends StatelessWidget {
  static String routeName = "/register_success";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(flexibleSpace: Image(
          image: AssetImage("assets/images/smarthome-bg.png"),
          fit: BoxFit.cover,
        ),
        backgroundColor: Colors.transparent,
      
        leading: SizedBox(),
        //title: Text("Success"),
      ),
      body: Body(),
    );
  }
}
