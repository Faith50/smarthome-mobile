import 'package:flutter/material.dart';
import 'package:smart_home_app/components/default_button.dart';
import 'package:smart_home_app/screens/home/home_screen_page.dart';
import 'package:smart_home_app/size_config.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:smart_home_app/screens/customer/customer_home_screen.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          constraints: BoxConstraints.expand(),
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/smarthome-bg.png"),
              fit: BoxFit.cover,
            ),
          ),
        child: Center(child: Column(
          children:<Widget>[

        SizedBox(height: SizeConfig.screenHeight * 0.1),
        Image.asset(
          "assets/images/success.png",
          height: SizeConfig.screenHeight * 0.2, //40%
        ),
        SizedBox(height: SizeConfig.screenHeight * 0.08),
        Text(
          "Register Successful!", style: GoogleFonts.lato(fontStyle: FontStyle.normal, color: Colors.black45, fontSize: getProportionateScreenWidth(30),),),
SizedBox(height: SizeConfig.screenHeight * 0.04),
       Text(
          "Welcome to Smarthomes, here we will connect you \nto the right Artisans nearest to you!", style: GoogleFonts.lato(fontStyle: FontStyle.normal, color: Color(0xFF03a54f)),
          textAlign: TextAlign.center,),

        Spacer(),
        SizedBox(
          width: SizeConfig.screenWidth * 0.6,
          child: DefaultButton(
            text: "Proceed to Home",
            press: () {
              Navigator.push( context, MaterialPageRoute(builder: (context) => CustomerHomeScreen()));
            },
          ),
        ),
        Spacer(),
      ])),
    ));
  }
}
