import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../size_config.dart';



class WalletScreen extends StatelessWidget {
  static String routeName = "/wallet";
  @override
  Widget build(BuildContext context) {
final screenHeight = MediaQuery.of(context).size.height;

   return Scaffold(
      appBar: AppBar(
         leading: Icon(Icons.arrow_back, color: Colors.white),
      centerTitle: false,
        backgroundColor: Color(0xFF03a54f),
        title: Text('Wallet Dashboad', style: TextStyle(color: Colors.white, fontSize: 16, )),
      ),
    body: CustomScrollView(
      
        physics: ClampingScrollPhysics(),
        
        slivers: <Widget>[
          
          _buildHeader(screenHeight),
          _buildBody(screenHeight),
         
          //_buildYourOwnTest(screenHeight),
        ],
      ),
          backgroundColor: Color(0xFFc7e6c9),

      );
  }

   SliverToBoxAdapter _buildHeader(double screenHeight) {
    return SliverToBoxAdapter(
      child: Container(
        padding: const EdgeInsets.all(20.0),
        decoration: BoxDecoration(
          color: Color(0xFF03a54f),
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(40.0),
            bottomRight: Radius.circular(40.0),
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(mainAxisAlignment: MainAxisAlignment.center, 
            children: <Widget>[
              Center(
                  child: Padding(
                    padding: EdgeInsets.all(5.0),
                    child: Text(r"$ " "95,940.00",
                        style: TextStyle(color: Colors.white, fontSize: 24.0)),
                  ),
                ),
                                ]),
          ],
        ),
      ),
    );
  }
 SliverToBoxAdapter _buildBody(double screenHeight) {
    return SliverToBoxAdapter(
      
      child: SizedBox(
        width: double.infinity,
        
        child: Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(15)),
          child: SingleChildScrollView(
            child: Column(
              children: [
            
                  Container(
                  height: 400.0,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(15),
                          topRight: Radius.circular(15), )),
                  child: Padding(
                    padding: const EdgeInsets.all(5.0),
                    child:  Container(
          padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 2.0),
          child: GridView.count(
            crossAxisCount: 2,
            padding: EdgeInsets.all(3.0),
            children: <Widget>[
              makeDashboardItem("Withdraw", Icons.money,),
              makeDashboardItem("Deposit", Icons.approval),
              makeDashboardItem("History", Icons.history),
              makeDashboardItem("Pending Payment", Icons.pending),
            ],
          ),
        ),
                
                  ))],
            ),
          ),
        ),
      ),
    );
  }

  Card makeDashboardItem(String title, IconData icon) {
    return Card(
        elevation: 1.0,
        margin: new EdgeInsets.all(8.0),
        child: Container(
          decoration: BoxDecoration(
            boxShadow: [
          BoxShadow(
             offset: Offset(10, 10),
            color: Colors.black38,
            blurRadius: 5.0,
          ),
        ],
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          stops: [0.3, 1],
          colors: [Color(0xffffffff), Color(0xffffffff)]
          )),
          child: new InkWell(
            onTap: () {},
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.min,
              verticalDirection: VerticalDirection.down,
              children: <Widget>[
                SizedBox(height: 50.0),
                Center(
                    child: Icon(
                  icon,
                  size: 40.0,
                  color: Color(0xFF03a54f),
                )),
                SizedBox(height: 20.0),
                 Center(
                  child:  Text(title,
                      style:
                           TextStyle(fontSize: 18.0, color: Color(0xFF03a54f), fontWeight: FontWeight.w700)),
                )
              ],
            ),
          ),
        ));
  }
}