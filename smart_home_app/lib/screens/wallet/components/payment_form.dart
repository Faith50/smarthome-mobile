import 'package:flutter/material.dart';
import 'package:smart_home_app/components/no_account_text.dart';
import 'package:smart_home_app/components/socal_card.dart';
import '../../../size_config.dart';
import 'wallet_form.dart';
import 'package:smart_home_app/widgets/head.dart';
import 'package:get/get.dart';
import 'package:smart_home_app/widgets/widgets.dart';


class PaymentForm extends StatelessWidget {
  

  @override
  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }
    return result;
  }

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
     // appBar: CustomAppBar(),
      body: CustomScrollView(
        physics: ClampingScrollPhysics(),
        slivers: <Widget>[
          _buildHeader(screenHeight),
          _buildBody(screenHeight),
         
          //_buildYourOwnTest(screenHeight),
        ],
      ),
    );
  }

  SliverToBoxAdapter _buildHeader(double screenHeight) {
    return SliverToBoxAdapter(
      child: Container(
        padding: const EdgeInsets.all(20.0),
        decoration: BoxDecoration(
          color: Color(0xFF03a54f),
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(40.0),
            bottomRight: Radius.circular(40.0),
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
              Image.asset(
                "assets/images/smart-homes-logo-white.png",
                width: Get.width * 0.3,
                height: Get.height * 0.2,
              ),
            ]),
          ],
        ),
      ),
    );
  }

 SliverToBoxAdapter _buildBody(double screenHeight) {
    return SliverToBoxAdapter(
      
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: SingleChildScrollView(
            child: Column(
              children: [
            
                SizedBox(height: SizeConfig.screenHeight * 0.08),
                WalletForm(),
                SizedBox(height: SizeConfig.screenHeight * 0.08),
                
                
              ],
            ),
          ),
        ),
      ),
    );
  }
}
