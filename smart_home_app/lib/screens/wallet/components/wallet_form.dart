import 'package:flutter/material.dart';
import 'package:smart_home_app/components/custom_surfix_icon.dart';
import 'package:smart_home_app/components/form_error.dart';
import 'package:smart_home_app/helper/keyboard.dart';
import 'package:smart_home_app/screens/forgot_password/forgot_password_screen.dart';
import 'package:smart_home_app/screens/login_success/login_success_screen.dart';
import '../../../components/default_button.dart';
import '../../../constants.dart';
import '../../../size_config.dart';
//import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
//import 'package:intl/intl.dart';

class WalletForm extends StatefulWidget {

  @override

  _WalletFormState createState() => _WalletFormState();
}

class _WalletFormState extends State<WalletForm> {
  
  final dateController = TextEditingController();

  final _formKey = GlobalKey<FormState>();
  String PaymentType;
  String CardType;
  String CardNumber;
  String CardHolderName;
  String ExpiredDate;
  String CardCvv;
 bool save = false;
  final List<String> errors = [];


  void addError({String error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error); 
      });
  }
 @override
  void dispose() {
    // Clean up the controller when the widget is removed
    dateController.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [ 
          buildPaymentTypeFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildCardTypeFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildCardNumberFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildCardHolderNameFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildExpiredDateFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildCardCvvFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          
          Row(
            children: [
              Checkbox(
                value: save,
                activeColor: kPrimaryColor,
                onChanged: (value) {
                  setState(() {
                    save = value;
                  });
                },
              ),
              Text("Save information for future payment"),
              Spacer(),
            
            ],
          ),
          
          FormError(errors: errors),
          SizedBox(height: getProportionateScreenHeight(20)),
          DefaultButton(
            text: "Proceed Payment",
            press: () {
              if (_formKey.currentState.validate()) {
                _formKey.currentState.save();
                // if all are valid then go to success screen
                KeyboardUtil.hideKeyboard(context);
                Navigator.pushNamed(context, LoginSuccessScreen.routeName);
              }
            },
          ),
        ],
      ),
    );
  }

  TextFormField buildCardCvvFormField() {
    return TextFormField(
      keyboardType: TextInputType.number,
      onSaved: (newValue) => CardCvv = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kCvvNullError);
        } else if (value.length >= 3) {
          removeError(error: kShortCvvError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kCvvNullError);
          return "";
        } else if (value.length < 3) {
          addError(error: kShortCvvError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        
        labelText: "Card CVV",
        hintText: "Enter your CVV",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomPrefixIcon(svgIcon: "assets/icons/Flash Icon.svg"),
      ),
    );
  }


TextFormField buildExpiredDateFormField() {
  
    return TextFormField(
      readOnly: true,
         controller: dateController,
      keyboardType: TextInputType.text,
         
      decoration: InputDecoration(
        
        labelText: "Expired Date",
        hintText: "Pick your Date",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomPrefixIcon(svgIcon: "assets/icons/Conversation.svg"),
      ),
      onTap: () async {
        var date =  await showDatePicker(
              context: context, 
              initialDate:DateTime.now(),
              firstDate:DateTime(1900),
              lastDate: DateTime(2100));
        dateController.text = date.toString().substring(0,10);      
       }
    );
  }
  TextFormField buildCardHolderNameFormField() {
    return TextFormField(
      keyboardType: TextInputType.text,
         
      decoration: InputDecoration(
        labelText: "Card Holder Name",
        hintText: "Enter your Card Holder",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomPrefixIcon(svgIcon: "assets/icons/Discover.svg"),
      ),
    );
  }
  TextFormField buildCardNumberFormField() {
    return TextFormField(
      keyboardType: TextInputType.number,
         
      decoration: InputDecoration(
        labelText: "Card Number",
        hintText: "Enter your Card Number",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomPrefixIcon(svgIcon: "assets/icons/Plus Icon.svg"),
      ),
    );
  }
TextFormField buildCardTypeFormField() {
    return TextFormField(
      keyboardType: TextInputType.text,
         
      decoration: InputDecoration(
        labelText: "Card Type",
        hintText: "Enter your Card Type",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomPrefixIcon(svgIcon: "assets/icons/Cash.svg"),
      ),
    );
  }
  TextFormField buildPaymentTypeFormField() {
    return TextFormField(
      keyboardType: TextInputType.text,
         
      decoration: InputDecoration(
        labelText: "Payment Type",
        hintText: "Enter your Payment Type",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomPrefixIcon(svgIcon: "assets/icons/Bill Icon.svg"),
      ),
    );
  }
  
}
