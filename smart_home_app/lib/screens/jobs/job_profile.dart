import 'package:flutter/material.dart';

import 'components/body.dart';

class JobProfileScreen extends StatelessWidget {
  static String routeName = "/jobs";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Jobs'),
      ),
      body: Body(),
      
    );
  }
}