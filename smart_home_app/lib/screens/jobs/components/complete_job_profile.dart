import 'package:flutter/material.dart';
import 'package:smart_home_app/components/custom_surfix_icon.dart';
import 'package:smart_home_app/components/default_button.dart';
import 'package:smart_home_app/components/Theme.dart';
import 'package:smart_home_app/components/form_error.dart';
import 'package:smart_home_app/screens/otp/otp_screen.dart';
import 'package:smart_home_app/widgets/navbar.dart';
import 'package:smart_home_app/widgets/drawer.dart';
//import 'package:smart_home_app/screens/home/home_screen_page.dart';

import '../../../constants.dart';
import '../../../size_config.dart';

class JobUpload extends StatefulWidget {
  @override
  _JobUploadState createState() => _JobUploadState();
}

class _JobUploadState extends State<JobUpload> {
  final _formKey = GlobalKey<FormState>();
  final List<String> errors = [];
  String jobTitle;
  String jobDescription;
  


  void addError({String error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
    //drawer: NowDrawer(currentPage: "Profile"),
    body: Form(
      key: _formKey,
      child: Column(
        children: [
          buildJobTitleFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildJobDescriptionFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          
          FormError(errors: errors),
          SizedBox(height: getProportionateScreenHeight(40)),
          DefaultButton(
            text: "continue",
            press: () {
              if (_formKey.currentState.validate()) {
               // Navigator.pushNamed(context, HomeScreen.routeName);
              }
            },
          ),
        ],
      ),
    ));
  }
  TextFormField buildJobTitleFormField() {
    return TextFormField(
      onSaved: (newValue) => jobTitle = newValue,
      decoration: InputDecoration(
        labelText: "Job Title",
        hintText: "Enter Job Title",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomPrefixIcon(svgIcon: "assets/icons/Parcel.svg"),
      ),
    );
  }
     TextFormField buildJobDescriptionFormField() {
    return TextFormField(
      onSaved: (newValue) => jobDescription = newValue,
      decoration: InputDecoration(
        labelText: "Job Description",
        hintText: "Enter Job Description",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomPrefixIcon(svgIcon: "assets/icons/Gift Icon.svg"),
      ),
    );
  }
    
}
