import 'package:flutter/widgets.dart';
import 'package:smart_home_app/screens/jobs/job_profile.dart';
import 'package:smart_home_app/screens/cart/cart_screen.dart';
import 'package:smart_home_app/screens/complete_profile/complete_profile_screen.dart';
import 'package:smart_home_app/screens/details/details_screen.dart';
import 'package:smart_home_app/screens/forgot_password/forgot_password_screen.dart';
import 'package:smart_home_app/screens/home/home_screen_page.dart';
import 'package:smart_home_app/screens/login_success/login_success_screen.dart';
import 'package:smart_home_app/screens/otp/otp_screen.dart';
import 'package:smart_home_app/screens/profile/profile_screen.dart';
import 'package:smart_home_app/screens/sign_in/sign_in_screen.dart';
import 'package:smart_home_app/screens/secondsplash/second_splash_screen.dart';
import 'package:smart_home_app/screens/screen1/screen.dart';
import 'package:smart_home_app/screens/splash/splash_screen.dart';
import 'package:smart_home_app/screens/customer/customer_home_screen.dart';
import 'package:smart_home_app/screens/client_signup/client_sign_up_screen.dart';
import 'package:smart_home_app/screens/client_signin/client_sign_in_screen.dart';
import 'package:smart_home_app/screens/wallet/wallet_screen.dart';
import 'screens/sign_up/sign_up_screen.dart';
import 'package:smart_home_app/screens/sub_category/subcategory_screen.dart';

// We use name route
// All the routes will be available here
final Map<String, WidgetBuilder> routes = {
  ProfileScreen.routeName: (context) => ProfileScreen(),
  WalletScreen.routeName: (context) => WalletScreen(),
  SplashScreen.routeName: (context) => SplashScreen(),
  SecondSplashScreen.routeName: (context) => SecondSplashScreen(),
  ScreenNext.routeName: (context) => ScreenNext(),
  SignInScreen.routeName: (context) => SignInScreen(),
  ForgotPasswordScreen.routeName: (context) => ForgotPasswordScreen(),
  LoginSuccessScreen.routeName: (context) => LoginSuccessScreen(),
  SignUpScreen.routeName: (context) => SignUpScreen(),
  CompleteProfileScreen.routeName: (context) => CompleteProfileScreen(),
  OtpScreen.routeName: (context) => OtpScreen(),
  ClientSignInScreen.routeName: (context) => ClientSignInScreen(),
  DetailsScreen.routeName: (context) => DetailsScreen(),
  CartScreen.routeName: (context) => CartScreen(),
  ProfileScreen.routeName: (context) => ProfileScreen(),
  JobProfileScreen.routeName: (context) => JobProfileScreen(),
  ClientSignUpScreen.routeName: (context) => ClientSignUpScreen(),
CustomerHomeScreen.routeName: (context) => CustomerHomeScreen(),
SubCategoryScreen.routeName: (context) => SubCategoryScreen(),
};
